# Linkstatus

Provides a simple mechanism to check links.

## Usage

    const linkchecker = require("linkchecker");
    const reporter = (response) => { console.log(response.status); }
    const checker = linkchecker({reporter, maxSockets: 3, hostDelay: 1000}); 
    checker.enqueue("http://example.com/link1");
    checker.enqueue("http://example.com/link2");
    checker.enqueue("http://slashdot.org/");
    checker.run().then(() => {
       console.log("Checked all links");
    });

## Note on missing intermediate certificates workaround
Most moderns browsers performs AIA (Authority Information Access) chasing. That means that when intermediate certificates in a certificate chain is missing they try to load them. This is not done by libssl as it is left to the application layer. Unfortunately, node-fetch does not have a setting for this either, however there is a way to add extra certificates to the https module in node.

Hence, we introduce the boolean setting ```fixCA``` on the processor to do checks that are closer to what users experience in a browser setting.

To avoid making these requests every time we depend on a library (node_extra_ca_certs_mozilla_bundle) that downloads a bunch of trusted intermediary certificates based on what the mozilla browser does.