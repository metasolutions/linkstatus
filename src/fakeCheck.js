export default (url, timeoutInMilliseconds) => {
  const success = !Math.round(Math.random());
  const delay = Math.round(Math.random()*timeoutInMilliseconds);
  return new Promise((callback) => {
    setTimeout(() => {
      callback({
        requestStatus: "success",
        url,
        status: success ? 200 : 404,
        statusText: ''
      });
    }, delay);
  });
};