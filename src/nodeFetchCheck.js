import fetch, { AbortError } from 'node-fetch';
import http from 'node:http';
import https from 'node:https';
import fileSystem from 'node:fs';

let extra_ca_module = 'node_extra_ca_certs_mozilla_bundle/ca_bundle/ca_intermediate_root_bundle.pem';
let ca_bundle_path = new URL(`../node_modules/${extra_ca_module}`, import.meta.url);
if (!fileSystem.existsSync(ca_bundle_path)) {
// Running as a library in another context, load from parallell module
  ca_bundle_path = new URL(`../../${extra_ca_module}`, import.meta.url);
}
const ca_bundle = fileSystem.readFileSync(ca_bundle_path, 'utf-8');

const httpAgent = new http.Agent({ keepAlive: true });
const httpsAgent = new https.Agent({ keepAlive: true });
const agent = function(_parsedURL) {
  if (_parsedURL.protocol == 'http:') {
    return httpAgent;
  } else {
    return httpsAgent;
  }
};

const fetchMethod = async (url, method, timeoutInMilliseconds) => {
  // AbortController was added in node v14.17.0 globally
  let AbortController = globalThis.AbortController;
  if (!AbortController) {
    AbortController = await import('abort-controller');
    AbortController = AbortController.AbortController;
  }

  const controller = new AbortController();
  const t = setTimeout(() => {
    controller.abort();
  }, timeoutInMilliseconds || 5000);
  try {
    const response = await fetch(url, {
      method,
      redirect: 'follow',
      follow: 3,
 //     agent,
      signal: controller.signal,
      headers: {
        "User-Agent": "Link status checker, v1.0"}
    });
    if (method === 'HEAD' && response.status >= 400) {
      return fetchMethod(url, 'GET', timeoutInMilliseconds);
    }
    return response;
  } catch (error) {
    if (error instanceof AbortError) {
      throw new Error("timeout");
    }
    throw error;
  } finally {
    clearTimeout(t);
  }
};

const check = async (url, timeout, filePath, fixCA, method) => {
  const orig_ca = https.globalAgent.options.ca;
  if (fixCA) https.globalAgent.options.ca = ca_bundle;
  const httpMethod = filePath || method === 'GET' ? 'GET' : 'HEAD';
  const response = await fetchMethod(url, httpMethod, timeout);
  if (filePath) {
    await new Promise((resolve, reject) => {
      const dest = fileSystem.createWriteStream(filePath);
      response.body.pipe(dest);
      response.body.on("end", () => {
        dest.close();
        resolve()
      });
      dest.on("error", reject);
    });
  }
  https.globalAgent.options.ca = orig_ca;
  return {
    requestStatus: response.status >= 200 && response.status <= 300 ? "success" : "broken",
    url,
    status: response.status,
    statusText: response.statusText
  };
};

export default async (url, timeout, filePath, fixCA, method) => {
  try {
    // Try without fixCA first.
    return await check(url, timeout, filePath, false, method);
  } catch (err) {
    if (err.message === 'timeout') {
      return await check(url, timeout*4, filePath, false, method);
    } else if (fixCA &&
      (err.code === 'CERT_UNTRUSTED' ||
      err.code === 'UNABLE_TO_VERIFY_LEAF_SIGNATURE')) {
      return check(url, timeout, filePath, fixCA, method);
    }
    throw err;
  }
};
