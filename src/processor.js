import nodeFetchCheck from './nodeFetchCheck.js';
import fakeCheck from "./fakeCheck.js";

const domain = url => new URL(url).host;

export default (params) => {
  const { reporter, maxSockets = 5, hostDelay = 1000, dryRun = false, timeout = 5000, toFilePath, fixCA, method } = params;
  const site2urls = {};
  const sites = [];
  const times = [];
  const url2inquiries = new Map();

  /**
   * Returns the next link for site given by index.
   * If there are no more links for the given site, it removes the site.
   * If there are more links remaining for the site the current time is saved to make sure we can wait an appropriate delay.
   * @param idx
   * @return {string} a url
   */
  const nextLinkInSite = (idx) => {
    const site = sites[idx];
    const arr = site2urls[site];
    const url = arr.shift();
    if (arr.length === 0) {
      delete site2urls[site];
      sites.splice(idx, 1);
      times.splice(idx, 1);
    } else {
      times[idx] = Date.now();
    }
    return url;
  };
  /**
   * Finds the next url to check.
   * If all sites where checked recently (within the minimum delay) no url is returned.
   * @return {string}
   */
  const next = () => {
    const now = new Date();
    for (let idx = 0;idx < sites.length; idx++) {
      const t = times[idx];
      if (t === undefined || Date.now() - t > hostDelay) {
        return nextLinkInSite(idx);
      }
    }
  };

  /**
   * Waits the minimal time needed for a minimum of delay has passed for one of the sites.
   * @return {Promise<void>}
   */
  const waitForDelay = async () => {
    if (sites.length > 0) {
      let earliest = times[0];
      times.forEach(t => {
        if (t < earliest) {
          earliest = t;
        }
      });
      await new Promise((callback) => {
        setTimeout(callback, hostDelay + earliest - Date.now() + 5);
      });
    }
  };

  const linkCheck = [];
  /**
   * Start the link check
   * @param url the url to check
   * @param idx the position of the linkCheck in the array of concurrent link checks allowed
   */
  const doLinkCheck = (url, idx) => {
    linkCheck[idx] = url;
    (dryRun ? fakeCheck: nodeFetchCheck)(url, timeout, toFilePath ? toFilePath(url) : undefined, fixCA, method).then((obj) => {
      url2inquiries.get(url).forEach((inquiry) => {
        reporter(obj, inquiry);
      });
      url2inquiries.delete(url);
      delete linkCheck[idx];
    }, (err) => {
      url2inquiries.get(url).forEach((inquiry) => {
        reporter({
          requestStatus: 'broken',
          statusText: err.message,
          url
        }, inquiry);
      });
      delete linkCheck[idx];
    });
  };

  const process = async () => {
    for (let i = 0; i < maxSockets; i++) {
      const lc = linkCheck[i];
      if (!lc && sites.length > 0) {
        let nextLink = next();
        if (!nextLink) {
          await waitForDelay();
          nextLink = next();
        }
        doLinkCheck(nextLink, i);
      }
    }
    await new Promise((callback) => {
      setTimeout(callback, 100);
    });
  };

  return {
    enqueue(url, inquiry) {
      let inquiries = url2inquiries.get(url);
      if (!inquiries) {
        url2inquiries.set(url, [inquiry]);
      } else {
        inquiries.push(inquiry);
        return;
      }

      const d = domain(url);
      let arr = site2urls[d];
      if (!arr) {
        sites.push(d);
        arr = [];
        site2urls[d] = arr;
      }
      arr.push(url);
    },
    run: async () => {
      while (sites.length > 0) {
        await process();
      }

      await new Promise((cb) => setTimeout(cb, 7000));
    }
  }
};