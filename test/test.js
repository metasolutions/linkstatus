import processor from '../src/processor.js';

const reporter = (linkReport) => {
  if (linkReport.requestStatus === "success") {
    console.log(`${linkReport.status}: ${linkReport.url}`);
  } else {
    console.log(`More serious issue with ${linkReport.url}`);
  }
}

const checker = processor({
  reporter,
  maxSockets: 5,
  hostDelay: 100,
  //dryRun: true
});

/*for (let i = 0;i<10;i++) {
  checker.enqueue(`http://example.com/link${i}`);
}
for (let i = 0;i<10;i++) {
  checker.enqueue(`http://fakeaddressnotexisting.com/link${i}`);
}*/
checker.enqueue("http://entryscape.com/", {});
checker.enqueue("http://entryscape.com/", {});
checker.enqueue("http://entryscape.com/", {});
checker.run().then(() => {
  console.log("Checked all links");
});